
// Expression bodies
/** These are the odds */
var odds = evens.map(v => v + 1);

/** These are the nums */
var nums = evens.map((v, i) => v + i);

/** These are the pairs */
var pairs = evens.map(v => ({even: v, odd: v + 1}));

// Statement bodies
nums.forEach(v => {
  if (v % 5 === 0)
    fives.push(v);
});

// Lexical this
/** This is my friend Bob. */ 
var bob = {
  _name: "Bob",
  _friends: [],
  printFriends() {
    this._friends.forEach(f =>
      console.log(this._name + " knows " + f));
  }
}

/** Function to print something on the console. */
function print(){
	console.log("i printed");
}

/** Function to do some math problems. */
function math(){
	console.log("I can say that 4 + 5 = " + 4+5);
}

module.exports = {
}