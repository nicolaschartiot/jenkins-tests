// -----------------------------------------------------------------

// Plugins

// -----------------------------------------------------------------

var gulp  		= require ("gulp");
var gutil  		= require ("gulp-util");
var source  	= require ("vinyl-source-stream");
var jsdoc 		= require ("gulp-jsdoc3");
var babel 		= require ("gulp-babel");
var browserSync = require("browser-sync");
var del 		= require('del');
var browserify 	= require('browserify');
var mocha 		= require('gulp-mocha');
var cleanCSS 	= require('gulp-clean-css');
var minify 		= require('gulp-minify');





// -----------------------------------------------------------------

// Default

// -----------------------------------------------------------------

gulp.task('default', ['clean', 'minify', 'mocha', 'jsdoc', 'browserify', 'copyIndex', 'browserSync', 'watchFiles', 'babelify'])




// -----------------------------------------------------------------

// JSDoc

// -----------------------------------------------------------------

gulp.task('jsdoc', function (cb) {
    gulp.src('./src/app.js', {read: false})
        .pipe(jsdoc(cb));
});




// -----------------------------------------------------------------

// BabelJS

// -----------------------------------------------------------------

gulp.task('babelify', function(){
	return gulp.src('src/app.js')
		.pipe(babel({
			presets: ['es2015']
		}))
		.pipe(gulp.dest('./build'))
		.pipe(browserSync.reload({stream: true}));
});




// -----------------------------------------------------------------

// Copy Index

// -----------------------------------------------------------------

gulp.task('copyIndex', function () {
	gulp.src('src/index.html')
		.pipe(gulp.dest('./build'))
		.pipe(browserSync.reload({stream:true}));
	gulp.src('src/**/*.css')
		.pipe(gulp.dest('./build'));
	gulp.src('./src/**.jpg')
		.pipe(gulp.dest('./build'));
});




// -----------------------------------------------------------------

// Browser Sync

// -----------------------------------------------------------------

gulp.task('browserSync', function()
{
	browserSync({
		server: {
			baseDir: './build'
		}
	});
});




// -----------------------------------------------------------------

// Watch Files

// -----------------------------------------------------------------

gulp.task('watchFiles', function () {
	gulp.watch('src/index.html', ['copyIndex'])
	gulp.watch('src/app.js', ['babelify'])
});




// -----------------------------------------------------------------

// Clean

// -----------------------------------------------------------------

gulp.task('clean', function(cb) {
    del(['./build'], cb);
});




// -----------------------------------------------------------------

// Browserify

// -----------------------------------------------------------------

gulp.task('browserify', function () 
{
	browserify('./**/*.js')
		.bundle()
		.on('error', function(e)
		{
			//gutil.log(e);
		})
		.pipe(source('bundle.js'))
		.pipe(gulp.dest('./'))
});




// -----------------------------------------------------------------

// Mocha 

// -----------------------------------------------------------------

gulp.task('mocha', function () {
	return gulp.src('./test/test.js', {read: false})
		// gulp-mocha needs filepaths so you can't have any plugins before it 
		.pipe(mocha({reporter: 'nyan'}));
});




// -----------------------------------------------------------------

// Minify

// -----------------------------------------------------------------

gulp.task('minify', function() {
  // return gulp.src('src/*.css')
  //   .pipe(cleanCSS({compatibility: 'ie8'}))
  //   .pipe(gulp.dest('./build'));
  gulp.src('src/material.js')
    .pipe(minify({
        ext:{
            src:'-debug.js',
            min:'.js'
        },
        exclude: ['tasks'],
        ignoreFiles: ['.combo.js', '-min.js']
    }))
    .pipe(gulp.dest('./build'))
      
});






