(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){

// Expression bodies
/** These are the odds */
var odds = evens.map(v => v + 1);

/** These are the nums */
var nums = evens.map((v, i) => v + i);

/** These are the pairs */
var pairs = evens.map(v => ({even: v, odd: v + 1}));

// Statement bodies
nums.forEach(v => {
  if (v % 5 === 0)
    fives.push(v);
});

// Lexical this
/** This is my friend Bob. */ 
var bob = {
  _name: "Bob",
  _friends: [],
  printFriends() {
    this._friends.forEach(f =>
      console.log(this._name + " knows " + f));
  }
}

/** Function to print something on the console. */
function print(){
	console.log("i printed");
}

/** Function to do some math problems. */
function math(){
	console.log("I can say that 4 + 5 = " + 4+5);
}

module.exports = {
}
},{}]},{},[1]);
