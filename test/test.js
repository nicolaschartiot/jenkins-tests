var expect = require('chai').expect;
var should = require('chai').should();
var assert = require('chai').assert;



//-----------------------------------------------
// Expect
//-----------------------------------------------

describe('Expect test', function () {
	it('Expect that true is equal to true', function () {
		expect(true).to.be.true;
	});
});



//-----------------------------------------------
// Should
//-----------------------------------------------

var foo = "true";

describe('Should test', function () {
	it('True should be equal to true', function () {
		foo.should.equal('true');
	});
});



//-----------------------------------------------
// Assert
//-----------------------------------------------

var ffoo = 'bar'
var beverages = { tea: [ 'chai', 'matcha', 'oolong' ] };

describe('Assert test', function () {
	it('Should assert the beverages', function () {
		assert.lengthOf(beverages.tea, 3, 'beverages has 3 types of tea');
	});
});
